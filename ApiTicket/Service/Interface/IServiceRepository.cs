﻿namespace ApiTicket.Controllers
{
    public interface IServiceRepository
    {
        ITicketRepository TicketRepository { get; }
        void Commit();
    }
}
