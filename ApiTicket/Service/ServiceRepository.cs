﻿namespace ApiTicket.Controllers
{
    public class ServiceRepository : IServiceRepository
    {
        private TicketRepository _ticketsRepository;

        public AppDbContext _dbContex;
        public ServiceRepository(AppDbContext dbContext)
        {
            _dbContex = dbContext;
        }
        public ITicketRepository TicketRepository => _ticketsRepository ??= new TicketRepository(_dbContex);

        public void Commit()
        {
            _dbContex.SaveChanges();
        }
        public void Dispose()
        {
            _dbContex.Dispose();
        }
    }
}
