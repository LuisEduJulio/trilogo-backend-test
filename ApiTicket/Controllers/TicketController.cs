﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace ApiTicket.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("api/[Controller]")]
    //[EnableCors("PermitirApiRequest")
    public class TicketController : ControllerBase
    {
        private readonly IServiceRepository _services;
        public TicketController(IServiceRepository services)
        {
            _services = services;
        }
        [HttpGet]
        public ActionResult<List<Ticket>> Get()
        {
            return _services.TicketRepository.GetTickets().ToList();
        }
        [HttpGet("{id}", Name = "ObterTicket")]
        public ActionResult<Ticket> GetId(int id)
        {
            var ticket = _services.TicketRepository.GetById(a => a.Id == id);

            if (ticket == null)
            {
                return NotFound();
            }
            return ticket;
        }
        [HttpPost]
        public ActionResult Post([FromBody] Ticket ticket)
        {
            _services.TicketRepository.Add(ticket);
            _services.Commit();

            return new CreatedAtRouteResult("ObterTicket",
              new { id = ticket.Id }, ticket);
        }
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Ticket ticket)
        {
            if (id != ticket.Id)
            {
                return BadRequest();
            }

            _services.TicketRepository.Update(ticket);
            _services.Commit();

            return Ok();
        }
        [HttpDelete("{id}")]
        public ActionResult<Ticket> Delete(int id)
        {
            var ticket = _services.TicketRepository.GetById(a => a.Id == id);

            if (ticket == null)
            {
                return NotFound();
            }
            _services.TicketRepository.Delete(ticket);
            _services.Commit();

            return ticket;
        }

    }
}
