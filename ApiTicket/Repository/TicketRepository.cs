﻿using System.Collections.Generic;
using System.Linq;

namespace ApiTicket.Controllers
{
    public class TicketRepository : Repository<Ticket>, ITicketRepository
    {
        public TicketRepository(AppDbContext context) : base(context)
        {

        }
        public IEnumerable<Ticket> GetTickets()
        {
            return Get().OrderBy(assignment => assignment.Data).ToList();
        }
    }
}
