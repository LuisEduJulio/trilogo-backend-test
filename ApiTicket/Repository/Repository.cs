﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace ApiTicket.Controllers
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected AppDbContext _dbcontext;

        public Repository(AppDbContext dbcontext)
        {
            _dbcontext = dbcontext;
        }
        public void Add(T entity)
        {
            _dbcontext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            _dbcontext.Set<T>().Remove(entity);
        }

        public IQueryable<T> Get()
        {
            return _dbcontext.Set<T>().AsNoTracking();
        }

        public T GetById(Expression<Func<T, bool>> predicate)
        {
            return _dbcontext.Set<T>().AsNoTracking().SingleOrDefault(predicate);
        }

        public void Update(T entity)
        {
            _dbcontext.Entry(entity).State = EntityState.Modified;
            _dbcontext.Set<T>().Update(entity);
        }
    }
}
