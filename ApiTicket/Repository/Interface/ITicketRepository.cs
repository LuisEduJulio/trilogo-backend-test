﻿using System.Collections.Generic;

namespace ApiTicket.Controllers
{
    public interface ITicketRepository : IRepository<Ticket>
    {
        IEnumerable<Ticket> GetTickets();
    }
}
