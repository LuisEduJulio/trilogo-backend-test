﻿using ApiTicket.Controllers;
using AutoMapper;

namespace ApiTicket.DTO.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Ticket, TicketDTO>().ReverseMap();
        }
    }
}
