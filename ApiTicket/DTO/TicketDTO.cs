﻿using System;

namespace ApiTicket.DTO
{
    public class TicketDTO
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string AuthorName { get; set; }
        public DateTime Data { get; set; }
    }
}
